<?php

return array(
    'host'    => 'localhost',
    'port'    => 9312,
    'timeout' => 30,
    'indexes' => array(
        'test1' => array('table' => 'texts', 'column' => 'id'),
    ),
    'mysql_server' => array(
        'host' => 'localhost',
        'port' => 9306
    )
);