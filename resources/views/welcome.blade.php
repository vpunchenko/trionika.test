<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .centered-table {
                display: block;
                margin-left: auto;
                margin-right: auto
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 45px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-top position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    Trionika Test Task
                </div>

              <div>
                  <form action="{{route('search')}}" method="post">
                      {{ csrf_field() }}
                  <input class="form-control form-control-lg" type="text" name="search" placeholder="Input search text">
                      <button type="submit" class="btn btn-primary">Search</button>
                  </form>

                  @if(isset($results))
                      @if( count($results) > 0)
                      <div  class="flex-center">
                      <table class="table">
                          <thead>
                          </thead>
                          <tbody>
                          <tr>
                        @foreach($results as $result)
                                  @if ($loop->iteration % 10 == 0)
                                      </tr><tr>
                                     @endif
                          <td><b>{{$result->id}}</b></td>
                        @endforeach
                          </tbody>
                      </table>
                      @else
                      <b>Нет результатов</b>
                  @endif
                          @endif
                      </div>
              </div>
            </div>
        </div>
    </body>
</html>
