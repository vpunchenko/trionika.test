<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TextModel extends Model
{

    public static $lastId;
    protected $table = 'texts';
    protected $primaryKey = 'id';
    protected $guarded = '';
    public $timestamps = false;


    public static function  setLastTextId()
    {
        $statement = DB::select("show table status like 'texts'");
        self::$lastId = $statement[0]->Auto_increment;
    }

    public static function getLastTextId() {
        return self::$lastId;
    }
}
