<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use sngrl\SphinxSearch\SphinxSearch;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        if ($request->has('search') && $request->get('search')) {
            $sphinx = new SphinxSearch();
            $sphinx->limit(1000, 0, 1000);
            $results = $sphinx->search($request->get('search'), 'test1')
                ->setFieldWeights(
                    array(
                        'id'  => 10,
                        'text'    => 8,
                        'author' => 1
                    )
                )
                ->setMatchMode(\Sphinx\SphinxClient::SPH_MATCH_ANY)
                ->setSortMode(\Sphinx\SphinxClient::SPH_SORT_RELEVANCE, "@weight DESC")
                ->get(true);
        } else {
            $results = [];
        }

        return view('welcome', array('results' => $results));
    }
}
