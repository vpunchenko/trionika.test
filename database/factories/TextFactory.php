<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TextModel;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;



$factory->define(TextModel::class, function (Faker $faker) {
    $id = TextModel::$lastId++;
    return [
        'text' =>  $faker->text(100) .
            " $id . $id*2 . $id*3 . $id*4 . $id*5 . $id*6 "
            . $faker->text(100),
        'category'     => $faker->text(100),
        'description'  => $faker->paragraph(3,  true),
        'author'       => $faker->paragraph(3,  true),
        'topic'        => $faker->paragraph(3,   true),
        'theme'        => $faker->paragraph(3,  true),
        'retelling'    => $faker->paragraph(3,  true),
    ];
});
