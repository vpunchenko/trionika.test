<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\TextModel::setLastTextId();

        factory(App\TextModel::class, 11000)->create();
    }

}
